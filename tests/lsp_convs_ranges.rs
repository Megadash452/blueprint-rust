use blueprint::utils;
use lsp_types::Position;
use utils::LinesRange;

#[test]
fn span_to_range() {
    let src = "string";

    assert_eq!(utils::span_to_range(src, 0..0), //
        LinesRange {
            start: Position { line: 0, character: 0 },
            end: Position { line: 0, character: 0 },
        }
    );
    assert_eq!(utils::span_to_range(src, 0..3), // str
        LinesRange {
            start: Position { line: 0, character: 0 },
            end: Position { line: 0, character: 3 },
        }
    );
    assert_eq!(utils::span_to_range(src, 0..src.len()), // string
        LinesRange {
            start: Position { line: 0, character: 0 },
            end: Position { line: 0, character: 6 },
        }
    );
}

#[test]
fn span_to_range_multiline() {
    let src = "\nstring\n";

    assert_eq!(utils::span_to_range(src, 0..0), //
        LinesRange {
            start: Position { line: 0, character: 0 },
            end: Position { line: 0, character: 0 },
        }
    );
    assert_eq!(utils::span_to_range(src, 0..1), // \n (first)
        LinesRange {
            start: Position { line: 0, character: 0 },
            end: Position { line: 1, character: 0 },
        }
    );
    assert_eq!(utils::span_to_range(src, 1..7), // string
        LinesRange {
            start: Position { line: 1, character: 0 },
            end: Position { line: 1, character: 6 },
        }
    );
    assert_eq!(utils::span_to_range(src, 7..8), // \n (last)
        LinesRange {
            start: Position { line: 1, character: 6 },
            end: Position { line: 2, character: 0 },
        }
    );
    assert_eq!(utils::span_to_range(src, 0..src.len()), // \nstring\n
        LinesRange {
            start: Position { line: 0, character: 0 },
            end: Position { line: 2, character: 0 },
        }
    );
}



#[test]
fn range_to_span() {
    let src = "string";

    assert_eq!( // 
        utils::range_to_span(src, LinesRange {
            start: Position { line: 0, character: 0 },
            end: Position { line: 0, character: 0 },
        }),
        0..0
    );
    assert_eq!( // str
        utils::range_to_span(src, LinesRange {
            start: Position { line: 0, character: 0 },
            end: Position { line: 0, character: 3 },
        }),
        0..3
    );
    assert_eq!( // string
        utils::range_to_span(src, LinesRange {
            start: Position { line: 0, character: 0 },
            end: Position { line: 0, character: 6 },
        }),
        0..src.len()
    );
}

#[test]
fn range_to_span_multiline() {
    let src = "\nstring\n";

    assert_eq!( // 
        utils::range_to_span(src, LinesRange {
            start: Position { line: 0, character: 0 },
            end: Position { line: 0, character: 0 },
        }),
        0..0
    );
    assert_eq!( // \n (first)
        utils::range_to_span(src, LinesRange {
            start: Position { line: 0, character: 0 },
            end: Position { line: 1, character: 0 },
        }),
        0..1
    );
    assert_eq!( // string
        utils::range_to_span(src, LinesRange {
            start: Position { line: 1, character: 0 },
            end: Position { line: 1, character: 6 },
        }),
        1..7
    );
    assert_eq!( // \n (last)
        utils::range_to_span(src, LinesRange {
            start: Position { line: 1, character: 6 },
            end: Position { line: 2, character: 0 },
        }),
        7..8
    );
    assert_eq!( // \nstring\n
        utils::range_to_span(src, LinesRange {
            start: Position { line: 0, character: 0 },
            end: Position { line: 2, character: 0 },
        }),
        0..src.len()
    );
}