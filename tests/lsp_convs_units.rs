use blueprint::utils;
use lsp_types::Position;

#[test]
fn index_to_pos() {
    let src = "string";
    // |string
    assert_eq!(utils::index_to_pos(src, 0), Position { line: 0, character: 0 });
    // str|ing
    assert_eq!(utils::index_to_pos(src, 3), Position { line: 0, character: 3 });
    // string|
    assert_eq!(utils::index_to_pos(src, src.len()), Position { line: 0, character: 6 });
}

#[test]
fn index_to_pos_multiline() {
    let src = "\nstring\n";
    // |\nstring\n
    assert_eq!(utils::index_to_pos(src, 0), Position { line: 0, character: 0 });
    // \n|string\n
    assert_eq!(utils::index_to_pos(src, 1), Position { line: 1, character: 0 });
    // \nstr|ing\n
    assert_eq!(utils::index_to_pos(src, 4), Position { line: 1, character: 3 });
    // \nstring|\n
    assert_eq!(utils::index_to_pos(src, 7), Position { line: 1, character: 6 });
    // \nstring\n|
    assert_eq!(utils::index_to_pos(src, src.len()), Position { line: 2, character: 0 });
}


#[test]
fn pos_to_index() {
    let src = "string";
    // |string
    assert_eq!(utils::pos_to_index(src, Position { line: 0, character: 0 }), 0);
    // str|ing
    assert_eq!(utils::pos_to_index(src, Position { line: 0, character: 3 }), 3);
    // string|
    assert_eq!(utils::pos_to_index(src, Position { line: 0, character: 6 }), src.len());
}

#[test]
fn pos_to_index_multiline() {
    let src = "\nstring\n";
    // |\nstring\n
    assert_eq!(utils::pos_to_index(src, Position { line: 0, character: 0 }), 0);
    // \n|string\n
    assert_eq!(utils::pos_to_index(src, Position { line: 1, character: 0 }), 1);
    // \nstr|ing\n
    assert_eq!(utils::pos_to_index(src, Position { line: 1, character: 3 }), 4);
    // \nstring|\n
    assert_eq!(utils::pos_to_index(src, Position { line: 1, character: 6 }), 7);
    // \nstring\n|
    assert_eq!(utils::pos_to_index(src, Position { line: 2, character: 0 }), src.len());
}