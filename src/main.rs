use blueprint;
use std::{fs::File, io::{self, Read, Write}, process::exit};
use std::path::PathBuf;
use clap::{Parser, Subcommand};

// TODO: remove all derive debugs when done debugging

#[derive(Parser)]
#[command(version)]
struct CompilerArgs {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Compile a blueprint file to GtkBuilder UI
    Compile {
        /// The path to the blueprint source. Use - for STDIN
        in_path: PathBuf,
        /// The path to the output file. If None, STDOUT is assumed
        #[arg(short, long)]
        out_path: Option<PathBuf>,
        /// Removes insignificant whitespace from output
        #[arg(short, long)]
        compressed: bool,
        /// Don't print ANSI-colored text, only plain text
        #[arg(short='p', long)]
        no_color: bool,
    },
    /// Open language server over stdio
    Lsp {
        /// Path to a file where the server will log to. Contents of the file will be over-written
        log_path: Option<PathBuf>
    }
}

fn main() {
    if let Err(error) = handle_args(CompilerArgs::parse()) {
        // use io::ErrorKind::*;

        match error.kind() {
            _ => eprintln!("{}", error)
        }

        match error.raw_os_error() {
            Some(code) => exit(code),
            None => exit(1)
        }
    }
}

fn handle_args(args: CompilerArgs) -> io::Result<()> {
    match args.command {
        Commands::Compile {
            in_path: input,
            out_path: output,
            compressed,
            no_color
        } => {
            // The input file content will end up here
            let mut src = String::new();

            if no_color {
                *blueprint::PRINT_COLOR.lock().unwrap() = false
            }

            if input.to_string_lossy() == "-" {
                io::stdin().read_to_string(&mut src)?;
            } else {
                File::open(&input)?.read_to_string(&mut src)?;
            }

            let compiled = blueprint::compile_blp(input, src, compressed)?;

            // If user does not provide an output path, use STDOUT
            if let Some(output) = output {
                File::open(output)?.write_all(compiled.as_bytes())?;
            } else {
                println!("{compiled}");
            }
        },
        Commands::Lsp { log_path } => blueprint::language_server(log_path)?
    }

    Ok(())
}