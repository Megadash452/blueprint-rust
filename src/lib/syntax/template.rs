use super::*;


#[derive(SyntaxNode)]
pub struct Template {
    pub class_name: Ident,
    pub parent_class: Option<Class>,
    
    pub properties: Vec<Property>,
    pub signals: Vec<Signal>,
    pub children: Vec<Child>,
    pub styles: Vec<Styles>,
    /// Starts from "template" keyword, Ends at closing curly bracket `}`.
    span: Span
}
impl Parse for Template {
    fn parse(ctx: &mut ParseContext) -> Result<Option<Self>> {
        let span_start = if let Some(token) = ctx.parse_slice::<Ident>("template")? {
            token.span().start
        } else {
            return Ok(None)
        };
        let class_name = ctx.parse_err::<Ident>()?;

        let parent_class = match ctx.parse_slice::<Punct>(":")? {
            Some(_) => match ctx.parse::<Class>()? {
                Some(class) => Some(class),
                None => {
                    ctx.errors.push(Error(
                        ErrorKind::Custom("Expected a parent Class".to_string()),
                        {
                            let prev_span = ctx.tokens.span().clone();
                            // Error should point between the : and {
                            prev_span.start..if let Some(token) = ctx.peek::<Punct>()? {
                                                token.span().end
                                            } else {
                                                prev_span.end
                                            }
                        }
                    ));
                    None
                }
            },
            None => None
        };

        let ObjectContent {
            properties,
            signals,
            children,
            styles,
            span: content_span
        } = match ctx.parse::<ObjectContent>()? {
            Some(content) => content,
            // ObjectContent::parse returns None only if opening curly brace was not found.
            None => return Err(ctx.expected_error::<Punct>("{"))
        };

        Ok(Some(Self { class_name, parent_class, properties, signals, children, styles, span: span_start..content_span.end }))
    }
}