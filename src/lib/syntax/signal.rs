use super::*;


#[derive(SyntaxNode)]
pub struct Signal {
    pub signal_name: Ident,
    pub handler: Ident,
    pub object_id: Option<Ident>,
    pub flags: Vec<Ident>,
    /// Starts from signal_name, Ends after semi-colon `;`.
    span: Span
}
impl Parse for Signal {
    fn parse(ctx: &mut ParseContext) -> Result<Option<Self>> {
        let mut peek = ctx.fork();

        let signal_name = match peek.parse::<Ident>()? {
            Some(token) => token,
            None => return Ok(None)
        };
        // joint Punct tokens
        if peek.parse_slice::<Punct>("=")? == None {
            return Ok(None)
        };

        ctx.merge(peek);

        // operator =>. if found `=`, must also have `>`
        ctx.parse_slice_err::<Punct>(">")?;

        let handler = ctx.parse_err::<Ident>()?;
        ctx.parse_slice_err::<Punct>("(")?;
        let object_id = ctx.parse::<Ident>()?;
        ctx.parse_slice_err::<Punct>(")")?;
        
        let mut flags = vec![];
        while let Some(token) = ctx.parse::<Ident>()? {
            flags.push(token);
        }        

        let span = signal_name.span().start..ctx.parse_slice_err::<Punct>(";")?.span().end;

        Ok(Some(Self { signal_name, handler, object_id, flags, span }))
    }
}
