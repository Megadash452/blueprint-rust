use super::*;


#[derive(SyntaxNode)]
pub struct Styles {
    pub css_classes: Vec<StrLit>,
    /// Starts from keyword "styles", Ends at closing square bracket `]`.
    span: Span
}
impl Parse for Styles {
    fn parse(ctx: &mut ParseContext) -> Result<Option<Self>> {
        use TokenTree as TT;

        let span_start = match ctx.parse_slice::<Ident>("styles")? {
            Some(token) => token.span().start,
            None => return Ok(None)
        };
        ctx.parse_slice_err::<Punct>("[")?;

        let mut css_classes = vec![];
        // Tells if a separator (comma `,` in this case) is found after an item.
        // Having 2 consecutive **items** with no **separator** in between results
        // in an [`Error`], and having 2 consecutive **separators** with no **item**
        // in between results in an error.
        let mut found_sep = true;
        
        // Until Some("]"), Separated( item: StrLit, sep: "," )
        while let Some(result) = ctx.tokens.next() {
            match result {
                // End
                Ok(TT::Punct(token)) if token.slice() == "]" => {
                    let span = span_start..token.span().end;
                    return Ok(Some(Self { css_classes, span }))
                },
                // Separator
                Ok(TT::Punct(token)) if token.slice() == "," => {
                    if css_classes.is_empty() {
                        ctx.errors.push(Error(
                            ErrorKind::Custom("Can't have a separator `,` at the beginning".to_string()),
                            token.span()
                        ))
                    } else if found_sep {
                        ctx.errors.push(Error(
                            ErrorKind::ConsecutiveSep(","),
                            token.span()
                        ))
                    } else {
                        found_sep = true
                    }
                },
                // Item
                Ok(TT::StrLit(token)) => {
                    if found_sep {
                        css_classes.push(token);
                        found_sep = false
                    } else {
                        ctx.errors.push(Error(
                            ErrorKind::Expected(Punct::kind(), Some(",".to_string())),
                            token.span()
                        ))
                    }
                },
                Ok(_) => ctx.errors.push(Error(
                    ErrorKind::UnexpectedTokens,
                    ctx.tokens.span().clone()
                )),
                Err(error) => ctx.errors.push(error)
            }
        }

        Err(ctx.expected_error::<Punct>("]"))
    }
}
