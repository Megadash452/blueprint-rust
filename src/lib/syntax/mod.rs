mod import;
mod property;
mod styles;
mod signal;
mod object;
mod template;

use std::collections::HashMap;
use std::rc::Rc;
use blueprint_derive::SyntaxNode;

use crate::tokens::{
    TokenStream, TokenTree, Token,
    StrLit, NumLit, Punct, Ident
};
use crate::errors::{Error, ErrorKind};
use crate::utils::{self, Result, Span};

pub use import::NamespaceImport;
pub use property::Property;
pub use styles::Styles;
pub use signal::Signal;
pub use object::{Object, Class, Child};
use object::ObjectContent;
pub use template::Template;


pub trait Parse: Sized {
    /// Checks if the next token is of the implementor type. 3 Things can be
    /// returned:
    /// 
    /// ### For [`Token`]s:
    ///  1. A token of this type if it matches the next token in [`TokenStream`].
    ///  2. Nothing, meaning that the next token is not of this type.
    ///  3. An [`Error`] if the next [`Token`] could not be built properly (e.g.
    ///     because of an unclosed delimiter).
    /// 
    /// ### For other structs part of the Abstract Syntax Tree:
    ///  1. An instance of this struct
    ///  2. Nothing
    ///  3. An [`Error`] if found a token that makes the tokens have to build the
    ///     struct (such as a *keyword* or *punctuation*), but couldn't because the
    ///     other tokens are not the correct types. For example, if parsing for
    ///     [`NamespaceImport`] and found `Ident` with slice `"using"` (aka keyword
    ///     "using"), and the next token is a `Punct`, the parser will return an
    ///     [`Error`] because the next token is supposed to be an `Ident`.
    ///      - If an error is returned, `TokenStream` will advance to the token the
    ///        error occurred so that the parser is not stuck trying to parse the
    ///        same tokens and getting an error.
    /// 
    /// By rule, every function that calls this that is also a kind of "parse"
    /// function, should only advance the [`TokenStream`] if it is successful.
    /// Eg. [`ParseContext::parse_slice`] only advances the [`TokenStream`] if
    /// the token is type T and has that specific slice.
    fn parse(ctx: &mut ParseContext) -> Result<Option<Self>>;
}

/// The `SyntaxNode` trait is implemented by structs that are built from parsing the
/// source code. The means that the struct has a position, or `span` in the
/// blueprint source.
pub trait SyntaxNode {
    /// The section of the source code that  is found.
    fn span(&self) -> Span;
}


pub struct ParseContext {
    pub tokens: TokenStream,
    pub errors: Vec<Error>
}
impl ParseContext {
    pub fn new(src: Rc<String>) -> Self {
        Self {
            tokens: TokenStream::new(src),
            errors: vec![]
        }
    }
    /// Similar to the syn::ParseBuffer::parse approach. All it does is call
    /// [`Parse::parse`].
    /// 
    /// Pass the entire `context` rather than just the [`TokenStream`] because
    /// syntax parsers should have the option to halt when an error occurs or ignore
    /// the error an push it to the `context`'s list of errors (this is common with
    /// Vec fields of syntax).
    #[inline]
    pub fn parse<T: Parse>(&mut self) -> Result<Option<T>> {
        T::parse(self)
    }
    #[inline]
    pub fn peek<T: Parse>(&self) -> Result<Option<T>> {
        self.fork().parse::<T>()
    }

    /// Calls [`parse`]. Throws [`ErrorKind::Expected`] error if is `None`.
    pub fn parse_err<T: Token>(&mut self) -> Result<T> {
        match self.parse::<T>()? {
            Some(token) => Ok(token),
            None => Err(Error(
                ErrorKind::Expected(T::kind(), None),
                {
                    let end = self.tokens.span().end;
                    let mut peek = self.fork();
                    match peek.tokens.next() {
                        Some(Ok(token)) => token.span(),
                        None => end..peek.tokens.pos(),
                        Some(Err(error)) => return Err(error),
                    }
                }
            ))
        }
    }

    /// Only advances [`TokenStream`] if is `Some` and token has the specified slice.
    pub fn parse_slice<T: Token>(&mut self, slice: &str) -> Result<Option<T>> {
        let mut peek = self.fork();
        if let Some(token) = peek.parse::<T>()? {
            if token.slice() == slice {
                *self = peek;
                return Ok(Some(token))
            }
        }

        Ok(None)
    }
    /// Calls [`parse_slice`]. Throws [`ErrorKind::Expected`] error if is `None`.
    pub fn parse_slice_err<T: Token>(&mut self, slice: &str) -> Result<T> {
        match self.parse_slice::<T>(slice)? {
            Some(token) => Ok(token),
            None => Err(self.expected_error::<T>(slice))
        }
    }


    pub fn fork(&self) -> Self {
        Self { tokens: self.tokens.clone(), errors: self.errors.clone() }
    }
    pub fn merge(&mut self, fork: Self) {
        *self = fork;
    }

    /// Gives an error with the expected position (span) of an expected [`Token`].
    pub(super) fn expected_error<T: Token>(&self, slice: &str) -> Error {
        Error(
            ErrorKind::Expected(T::kind(), Some(slice.to_owned())),
            {
                let mut peek = self.fork();
                match peek.tokens.next() {
                    Some(Ok(token)) => token.span(),
                    Some(Err(_)) | None => self.tokens.pos()..peek.tokens.pos()
                }
            }
        )
    }
}


// TODO: use syntax macros, similar to what the main blueprint project uses


/// Syntax root for Blueprint source
pub struct BlueprintSyntax {
    pub imports: Vec<NamespaceImport>,
    pub templates: Vec<Template>,
    pub objects: Vec<Object>,

    // src: String,
    // pub ctx: ParseContext,
    pub errors: Vec<Error>
}
impl BlueprintSyntax {
    pub fn new(src: Rc<String>) -> Self {
        let mut ctx = ParseContext::new(src.clone());

        // Zero Or More NamespaceImport
        let mut imports = vec![];
        loop {
            match ctx.parse::<NamespaceImport>() {
                Ok(Some(import)) => imports.push(import),
                Ok(None) => break,
                Err(error) => {
                    ctx.errors.push(error);
                    ctx.tokens.next();
                }
            }
        }

        // Until None, AnyOf[Template, Object]
        let mut objects = vec![];
        let mut templates = vec![];
        if let Err(error) = ctx.tokens.skip_space() {
            ctx.errors.push(error)
        }
        while !ctx.tokens.remaining_src().is_empty() {
            match ctx.parse::<Template>() {
                Ok(Some(template)) => {
                    templates.push(template);
                    continue
                },
                Err(error) => {
                    ctx.errors.push(error);
                    continue
                },
                Ok(None) => {}
            }

            match ctx.parse::<Object>() {
                Ok(Some(object)) => {
                    objects.push(object);
                    continue
                },
                Err(error) => {
                    ctx.errors.push(error);
                    continue
                },
                Ok(None) => {}
            }

            // when found neither template or object. Prevents infinite loop
            ctx.tokens.next();
            ctx.errors.push(Error(
                ErrorKind::UnexpectedTokens,
                ctx.tokens.span().clone()
            ))
        }


        let mut used_ids: HashMap<String, Span> = HashMap::new();
        // Validate Object IDs
        for id in objects.iter()
            .map(|obj| obj.objects())
            .flatten()
            .filter_map(|obj| obj.id.as_ref())
        {
            match used_ids.get(id.slice()) {
                Some(span) => ctx.errors.push(Error(
                    ErrorKind::UsedId(id.slice().into(), utils::index_to_pos(src.as_str(), span.start)),
                    id.span()
                )),
                None => { used_ids.insert(id.slice().to_string(), id.span()); }
            }
        }

        Self { imports, templates, objects, errors: utils::merge_consecutive_errors(ctx.errors, src.as_str()) }
    }

    /// Iterate over all object declarations (root, children, and properties).
    /// See [`Object::objects`].
    pub fn object_defs(&self) -> impl Iterator<Item = &Object> {
        self.objects.iter()
            .map(|obj| obj.objects())
            .flatten()
    }

    /// Looks for the definition/declaration of an [`Object`] with the specified **id**. Useful for ***LSP***.
    pub fn find_object_by_id(&self, id: &str) -> Option<&Object> {
        self.object_defs().find(|obj|
            match &obj.id {
                Some(obj_id) if obj_id.slice() == id => true,
                _ => false
            }
        )
    }
}