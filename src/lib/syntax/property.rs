use super::*;


#[derive(SyntaxNode)]
pub struct Property {
    pub name: Ident,
    pub value: PropertyValue,
    /// Starts from property name, Ends after property value (before semi-colon `;`).
    span: Span
}
impl Parse for Property {
    fn parse(ctx: &mut ParseContext) -> Result<Option<Self>> {
        let mut peek = ctx.fork();

        let name = match peek.parse::<Ident>()? {
            Some(token) => token,
            None => return Ok(None)
        };
        if peek.parse_slice::<Punct>(":")? == None {
            return Ok(None)
        };

        ctx.merge(peek);

        let value = match ctx.parse::<PropertyValue>()? {
            Some(value) => value,
            None => return Err(Error(
                ErrorKind::UnexpectedTokens,
                ctx.tokens.span().clone()
            ))
        };

        ctx.parse_slice_err::<Punct>(";")?;

        let span = name.span().start..value.span().end;
        Ok(Some(Self { name, value, span }))
    }
}

pub enum PropertyValue {
    Binding {
        source_object_id: Ident,
        source_property: Ident,
        flags: Vec<Ident>,
        /// Starts from keyword "bind", Ends at last flag.
        span: Span
    },
    /// Sign (`+` or `-`), and a Number Literal.
    Number(Option<Punct>, NumLit),
    String(StrLit),
    TranslatedString {
        context: Option<StrLit>,
        content: StrLit,
        /// Starts from translatable token (`_` or `C_`), Ends at closing parenthesis `)`.
        span: Span
    },

    Object(Object),
    /// Booleans (true or false) are also included
    EnumOrID(Ident),
}
impl PropertyValue {
    fn parse_binding(ctx: &mut ParseContext) -> Result<Option<Self>> {
        let mut span = if let Some(token) = ctx.parse_slice::<Ident>("bind")? {
            token.span().start..0
        } else {
            return Ok(None)
        };

        let source_object_id = ctx.parse_err::<Ident>()?;
        ctx.parse_slice_err::<Punct>(".")?;
        let source_property = ctx.parse_err::<Ident>()?;
        span.end = source_property.span().end;

        // could be empty
        let mut flags = vec![];
        while let Some(token) = ctx.parse::<Ident>()? {
            // will end up with the span of the last flag
            span.end = token.span().end;
            flags.push(token);
        }

        Ok(Some(Self::Binding { source_object_id, source_property, flags, span }))
    }
    fn parse_trans_str(ctx: &mut ParseContext) -> Result<Option<Self>> {
        if let Some(token) = ctx.parse_slice::<Ident>("C_")? {
            ctx.parse_slice_err::<Punct>("(")?;
            let context = Some(ctx.parse_err::<StrLit>()?);
            ctx.parse_slice_err::<Punct>(",")?;
            let content = ctx.parse_err::<StrLit>()?;
            
            let span = token.span().start..ctx.parse_slice_err::<Punct>(")")?.span().end;

            Ok(Some(Self::TranslatedString { context, content, span }))
        } else if let Some(token) = ctx.parse_slice::<Ident>("_")? {
            ctx.parse_slice_err::<Punct>("(")?;
            let content = ctx.parse_err::<StrLit>()?;
            
            let span = token.span().start..ctx.parse_slice_err::<Punct>(")")?.span().end;

            Ok(Some(Self::TranslatedString { context: None, content, span }))
        } else {
            Ok(None)
        }
    }
}
impl Parse for PropertyValue {
    fn parse(ctx: &mut ParseContext) -> Result<Option<Self>> {
        if let Some(variant) = Self::parse_binding(ctx)? {
            return Ok(Some(variant))
        }

        if let Some(token) = ctx.parse::<StrLit>()? {
            return Ok(Some(Self::String(token)))
        }
        if let Some(variant) = Self::parse_trans_str(ctx)? {
            return Ok(Some(variant))
        }

        // Sign is `+` or `-`, or only number with no sign
        let sign = match ctx.parse_slice::<Punct>("+")? {
            None => ctx.parse_slice::<Punct>("-")?,
            some => some
        };
        if let Some(token) = ctx.parse::<NumLit>()? {
            return Ok(Some(Self::Number(sign, token)))
        } else if let Some(sign) = sign {
            // If has sign + or -, the number is required
            return Err(Error(
                ErrorKind::Expected(NumLit::kind(), None),
                sign.span().clone()
            ))
        }

        let mut peek = ctx.fork();
        
        if let Some(token) = peek.parse::<Ident>()? {
            // Ensure it is not possibly an Object
            if let Some(_) = peek.parse_slice::<Punct>(";")? {
                ctx.tokens.next();
                // don't skip semi-colon, leave that to Property::parse
                return Ok(Some(Self::EnumOrID(token)))
            }
        }
        if let Some(object) = ctx.parse::<Object>()? {
            return Ok(Some(Self::Object(object)))
        }

        Err(Error(
            ErrorKind::Custom("Invalid PropertyValue".to_string()),
            ctx.tokens.span().clone()
        ))
    }
}
impl SyntaxNode for PropertyValue {
    fn span(&self) -> Span {
        match self {
            Self::Number(Some(sign), num) => sign.span().start..num.span().end,
            Self::Number(None, token) => token.span(),
            Self::String(token) => token.span(),
            Self::Object(token) => token.span(),
            Self::EnumOrID(token) => token.span(),
            Self::Binding { span, .. } => span.clone(),
            Self::TranslatedString { span, .. } => span.clone()
        }
    }
}
