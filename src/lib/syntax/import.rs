use super::*;


#[derive(SyntaxNode)]
pub struct NamespaceImport {
    pub namespace: Ident,
    pub version: NumLit,
    /// Starts from "using" keyword, Ends after version number.
    span: Span
}
impl Parse for NamespaceImport {
    fn parse(ctx: &mut ParseContext) -> Result<Option<Self>> {
        let span_start = if let Some(token) = ctx.parse_slice::<Ident>("using")? {
            token.span().start
        } else {
            return Ok(None)
        };
        let namespace = ctx.parse_err::<Ident>()?;
        let version = ctx.parse_err::<NumLit>()?;
        ctx.parse_slice_err::<Punct>(";")?;

        let span = span_start..version.span().end;
        Ok(Some(Self { namespace, version, span }))
    }
}
