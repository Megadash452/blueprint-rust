use super::{*, property::PropertyValue};


#[derive(SyntaxNode)]
pub struct Object {
    pub class: Class,
    pub id: Option<Ident>,

    pub properties: Vec<Property>,
    pub signals: Vec<Signal>,
    pub children: Vec<Child>,
    pub styles: Vec<Styles>,
    /// Starts from class, Ends at closing curly bracket `}`.
    span: Span
}
impl Object {
    /// Get an iterator over the [`Object`]s defined inside this one
    /// (gets from **children** and **properties** that have [`Object`]s as values).
    /// Credits to [fasterthanlime](https://fasterthanli.me/articles/recursive-iterators-rust#a-better-solution).
    /// 
    /// ## Order
    /// 
    /// This object (***self***) will always be the first [`Object`] in the iterator.
    /// Then, the iterator will yield objects in **children** in order of definition,
    /// and then the iterator will yield objects in **properties** in order of definition.
    /// In other words, the objects in this [`Object`]'s **children**
    /// will come before the objects in its **properties**.
    /// 
    /// This behavior happens recursively,
    /// so when an [`Object`] has a *child* and an object *property*,
    /// where both [`Object`]s have a *child* and an object *property*,
    /// the first child's property object will come before the first property's child object.
    /// 
    /// ## Example
    /// 
    /// ```blp
    /// W1 {
    ///     W2 {
    ///         W3 {}
    ///         // comes before W6, even though it's a  property
    ///         p: W4 {};
    ///     }
    ///     p: W5 {
    ///         W6 {}
    ///         p: W7 {};
    ///     };
    /// }
    /// ```
    /// 
    /// When calling `objects()` on `W1`,
    /// the iterator will yield objects in this order:
    /// 
    /// ```txt
    /// W1, W2, W3, W4, W5, W6, W7
    /// ```
    pub fn objects(&self) -> Box<dyn Iterator<Item = &Self> + '_> {
        Box::new(
            // yields itself
            Some(self).into_iter()
                .chain( // then child objects
                    self.children.iter()
                        .map(|child| child.object.objects())
                        .flatten(),
                ).chain( // then property objects
                    self.properties.iter()
                        .filter_map(|property| match &property.value {
                            PropertyValue::Object(obj) => Some(obj.objects()),
                            _ => None,
                        }).flatten(),
                )
        )
    }
}
impl Parse for Object {
    fn parse(ctx: &mut ParseContext) -> Result<Option<Self>> {
        let class = match ctx.parse::<Class>()? {
            Some(class) => class,
            None => return Ok(None)
        };
        let id = ctx.parse::<Ident>()?;

        let ObjectContent {
            properties,
            signals,
            children,
            styles,
            span: content_span
        } = match ctx.parse::<ObjectContent>()? {
            Some(content) => content,
            // ObjectContent::parse returns None if opening curly brace was not found.
            None => return if class.skip_validation || class.namespace != None {
                // If Object class is not just an Ident (meaning it has the dot)
                Err(ctx.expected_error::<Punct>("{"))
            } else {
                Ok(None)
            }
        };

        let span = class.span().start..content_span.end;

        Ok(Some(Self { class, id, properties, signals, children, styles, span }))
    }
}


#[derive(SyntaxNode)]
/// Class of an [`Object`]. E.g.: `Gtk.Box`.
pub struct Class {
    pub namespace: Option<Ident>,
    pub class: Ident,
    pub skip_validation: bool,
    span: Span
}
impl Parse for Class {
    fn parse(ctx: &mut ParseContext) -> Result<Option<Self>> {
        let (span, namespace, class, skip_validation)
         = if let Some(ident) = ctx.parse::<Ident>()? {
            match ctx.parse_slice::<Punct>(".")? {
                // namespace.class
                Some(_) => {
                    let class = ctx.parse_err::<Ident>()?;
                    ( ident.span().start..class.span().end,
                      Some(ident), class, false )
                },
                // class
                None => ( ident.span(),
                          None, ident, false )
            }
        } else if let Some(point) = ctx.parse_slice::<Punct>(".")? {
            // .class
            let class = ctx.parse_err::<Ident>()?;
            ( point.span().start..class.span().end,
              None, class, true )
        } else {
            return Ok(None)
        };

        Ok(Some(Self { namespace, class, skip_validation, span }))
    }
}


#[derive(SyntaxNode)]
/// Helper struct to parse template and object contents
pub(super) struct ObjectContent {
    pub properties: Vec<Property>,
    pub signals: Vec<Signal>,
    pub children: Vec<Child>,
    pub styles: Vec<Styles>,
    /// Starts at the opening curly bracket `{`, Ends at closing curly bracket `}`.
    pub span: Span
}
impl Parse for ObjectContent {
    fn parse(ctx: &mut ParseContext) -> Result<Option<Self>> {
        let mut span = if let Some(token) = ctx.parse_slice::<Punct>("{")? {
            token.span().start..0
        } else {
            return Ok(None)
        };

        // Until Some("}"), AnyOf[Property, Signal, Child]
        // parse properties, signals, and children until found closing brace `}`
        let mut properties = vec![];
        let mut signals = vec![];
        let mut children = vec![];
        let mut styles = vec![];
        if let Err(error) = ctx.tokens.skip_space() {
            ctx.errors.push(error)
        }
        loop {
            if let Some(closing) = ctx.parse_slice::<Punct>("}")? {
                span.end = closing.span().end;
                return Ok(Some(Self { properties, signals, children, styles, span }))
            }

            match ctx.parse::<Property>() {
                Ok(Some(property)) => {
                    properties.push(property);
                    continue
                },
                Err(error) => {
                    ctx.errors.push(error);
                    continue
                },
                Ok(None) => {}
            }

            match ctx.parse::<Signal>() {
                Ok(Some(signal)) => {
                    signals.push(signal);
                    continue
                },
                Err(error) => {
                    ctx.errors.push(error);
                    continue
                },
                Ok(None) => {}
            }

            match ctx.parse::<Styles>() {
                Ok(Some(style)) => {
                    styles.push(style);
                    continue
                },
                Err(error) => {
                    ctx.errors.push(error);
                    continue
                },
                Ok(None) => {}
            }

            match ctx.parse::<Child>() {
                Ok(Some(child)) => {
                    children.push(child);
                    continue
                },
                Err(error) => {
                    ctx.errors.push(error);
                    continue
                },
                Ok(None) => {}
            }
            
            match ctx.tokens.next() {
                Some(_) => ctx.errors.push(Error(
                    ErrorKind::UnexpectedTokens,
                    ctx.tokens.span().clone()
                )),
                None => return Err(ctx.expected_error::<Punct>("}"))
            }
        }
    }
}


/// A GObject that is a child of another GObject.
#[derive(SyntaxNode)]
pub struct Child {
    /// Child Type is `Optional`. Contains information that is used by the parent
    /// object on how the child should behave. These tokens should be parsed later
    /// because they are Widget-specific.
    pub child_type_tokens: Vec<TokenTree>,
    pub object: Object,
    /// Starts at the opening bracket `[` of child type (if no child type, then
    /// starts at Object's class), Ends at object's closing curly bracket `}`.
    span: Span
}
impl Parse for Child {
    fn parse(ctx: &mut ParseContext) -> Result<Option<Self>> {
        let mut child_type_tokens = vec![];
        let mut has_child_type_bracket = false;
        let mut span = 0..0;

        if let Some(type_opening) = ctx.parse_slice::<Punct>("[")? {
            span.start = type_opening.span().start;
            has_child_type_bracket = true;
            while let Some(result) = ctx.tokens.next() {
                match result? {
                    TokenTree::Punct(punct) if punct.slice() == "]" =>
                        break,
                    token => child_type_tokens.push(token)
                }
            }
            // TODO: error if closing ] not found
        }

        let object = if let Some(object) = ctx.parse::<Object>()? {
            object
        } else if has_child_type_bracket {
            return Err(Error(
                ErrorKind::Custom("Expected an object after a Child Type".to_string()),
                ctx.tokens.span().clone()
            ))
        } else {
            return Ok(None)
        };

        // Span of child starts at Object's class if it doesn't have a child type.
        if !has_child_type_bracket {
            span.start = object.span().start;
        }
        span.end = object.span().end;

        Ok(Some(Self { child_type_tokens, object, span }))
    }
}
