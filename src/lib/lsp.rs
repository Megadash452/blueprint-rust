use std::{cell::RefCell, fs::File, io::{Write, self}, path::PathBuf, collections::HashMap, rc::Rc};

use serde_json::{json, from_value as to_struct};
use lazy_static::lazy_static;
use lsp_server::{Connection, Message, Request, ProtocolError, Response, ErrorCode, Notification};
use lsp_types::{
    request::{ GotoDefinition, GotoDeclaration },
    notification::{ Notification as _, DidOpenTextDocument, DidCloseTextDocument, DidChangeTextDocument, PublishDiagnostics },
    Url, TextDocumentSyncKind, InitializeResult, ClientCapabilities, InitializeParams, PublishDiagnosticsParams, GotoDefinitionResponse, Location, LocationLink
};
use blueprint_lsp_macros::{method, capability, LSP};

use crate::tokens::{TokenStream, Token};
use crate::errors::Errors;
use crate::syntax::{BlueprintSyntax, SyntaxNode};
use crate::utils;

lazy_static! {
    /// Construct a strongly typed struct to verify the validity of the info.
    static ref INIT_INFO: InitializeResult = to_struct(json!({
        // Server capabilities will be defined per method with the `capability` macro
        "capabilities": *SERVER_CAPABILITIES,
        "serverInfo": {
            "name": "Blueprint",
            "version": "0.1"
        }
    })).expect("JSON Object could not be parsed into `InitializeResult`");
}


pub struct Server {
    logger: Option<RefCell<File>>,
    connection: Connection,
    client_capabilities: ClientCapabilities,
    documents: HashMap<Url, (Rc<String>, BlueprintSyntax)>
}
#[LSP]
impl Server {
    pub fn new(log_path: Option<PathBuf>) -> io::Result<Self> {
        let logger = match log_path {
            Some(path) => match File::create(path.clone()) {
                Ok(file) => Some(RefCell::new(file)),
                Err(error) => {
                    return Err(io::Error::new(
                        error.kind(),
                        format!("Error opening Log File {:?} in write mode: {}", path, error.to_string())
                    ))
                }
            },
            None => None
        };
        // will be logged to the server once it is initialized
        let mut log_buf = String::new();
        
        let (connection, __io_threads) = Connection::stdio();

        // Receive init response from client
        let client_capabilities = match connection.initialize_start() {
            Ok((id, init)) => {
                log_buf.push_str(&format!("Server INIT INFO: {}\n", json!(*INIT_INFO)));
                log_buf.push_str(&format!("Client INIT INFO: {init}\n"));
                // Send init response
                if let Err(error) = connection.initialize_finish(id, json!(*INIT_INFO)) {
                    return Err(io::Error::new(io::ErrorKind::Other, error.to_string()))
                }

                to_struct::<InitializeParams>(init)?.capabilities
            },
            Err(error) =>
                return Err(io::Error::new(io::ErrorKind::Other, error.to_string()))
        };

        let server = Self { logger, connection, client_capabilities, documents: HashMap::new() };

        server.log(log_buf);
        server.log("--- Initialized Server --");

        Ok(server)
    }

    pub fn log(&self, msg: impl AsRef<str>) {
        if let Some(logger) = &self.logger {
            let mut file = logger.borrow_mut();
            if let Err(_) = file.write(msg.as_ref().as_bytes())
                {return}
            if let Err(_) = file.write(&[b'\n'])
                {return}
            if let Err(_) = file.flush()
                {return}
        }
    }
    /// Create an [`io::Error`] with a message and log the message.
    pub fn error(&self, msg: impl AsRef<str>) -> io::Error {
        self.log(msg.as_ref());
        io::Error::new(io::ErrorKind::Other, msg.as_ref())
    }

    pub fn send(&self, msg: Message) {
        // log message
        match &msg {
            Message::Request(req) => 
                self.log(format!("Send Request: {}", serde_json::to_value(req).unwrap())),
            Message::Response(response) =>
                self.log(format!("Send Response: {}", serde_json::to_value(response).unwrap())),
            Message::Notification(notif) =>
                self.log(format!("Send Notification: {}", serde_json::to_value(notif).unwrap()))
        }

        if let Err(error) = self.connection.sender.send(msg) {
            self.log(format!("Error sending response: {error}"))
        }
    }
    pub fn receive(&self) -> Option<Message> {
        match self.connection.receiver.recv() {
            Ok(msg) => Some(msg),
            // recv() returns error when disconnected
            Err(_) => None
        }
    }

    #[inline(always)]
    pub fn handle_shutdown(&self, req: &Request) -> Result<bool, ProtocolError> {
        self.connection.handle_shutdown(req)
    }

    pub fn handle_request(&mut self, mut req: Request) -> io::Result<()> {
        // * Using #[LSP] as parent macro, Pupulate using #[method(req ...)] macro

        // Final Return
        Ok(self.send(Message::Response(Response::new_err(
            req.id, ErrorCode::MethodNotFound as i32, format!("Blueprint Server does not implement \"{}\"", req.method)
        ))))
    }
    pub fn handle_notification(&mut self, mut notif: Notification) -> io::Result<()> {
        // * Using #[LSP] as parent macro, Pupulate using #[method(notif ...)] macro

        // Final Return
        Ok(self.send(Message::Response(Response::new_err(
            0.into(), ErrorCode::MethodNotFound as i32, format!("Blueprint Server does not implement \"{}\"", notif.method)
        ))))
    }


    // No "initialize" LSP method. Initialization of the server happens in Self::new()

    #[method(notif DidOpenTextDocument)]
    #[capability("textDocumentSync": { "openClose": true })]
    pub fn on_open(&mut self, params: Method::Params) {
        let src = Rc::new(params.text_document.text);

        self.documents.insert(params.text_document.uri.clone(), (
            src.clone(),
            BlueprintSyntax::new(src)
        ));

        self.send_diagnostics(params.text_document.uri, 1);
    }

    #[method(notif DidCloseTextDocument)]
    #[capability("textDocumentSync": { "openClose": true })]
    pub fn on_close(&mut self, params: Method::Params) {
        self.documents.remove(&params.text_document.uri);
    }

    #[method(notif DidChangeTextDocument)]
    #[capability("textDocumentSync": { "change": TextDocumentSyncKind::INCREMENTAL })]
    pub fn on_change(&mut self, params: Method::Params) {
        for change in params.content_changes {
            let entry = self.documents.get_mut(&params.text_document.uri).unwrap();
            // The section in the current source code that will be replaced with the incoming change.
            let span = match change.range {
                Some(range) => utils::range_to_span(&entry.0, range),
                None => 0..entry.0.len()
            };

            entry.0 = Rc::new({
                let mut src = entry.0.as_str().to_owned();
                src.replace_range(span, &change.text);
                src
            });
            entry.1 = BlueprintSyntax::new(entry.0.clone());

            self.send_diagnostics(params.text_document.uri.clone(), params.text_document.version);
        }
    }

    
    #[method(req GotoDefinition)]
    #[capability("definitionProvider": true)]
    pub fn goto_definition(&self, params: Method::Params) -> Method::Result {
        use crate::tokens::TokenTree as TT;
        let uri = params.text_document_position_params.text_document.uri;
        let entry = &self.documents[&uri];
        let src = entry.0.as_str();
        let index = utils::pos_to_index(src, params.text_document_position_params.position);

        // find reference token at index
        let ref_ident = match TokenStream::new(entry.0.clone()).find(|result| {
            match result {
                Ok(TT::Ident(token))
                    if token.span().start <= index && token.span().end >= index => true,
                _ => false,
            }
        }) {
            Some(Ok(TT::Ident(token))) => token,
            _ => return None
        };
        // find object with that id
        let object = match entry.1.find_object_by_id(ref_ident.slice()) {
            Some(obj) => obj,
            None => return None
        };
        let pos = utils::span_to_range(src, object.span());


        use lsp_types::{TextDocumentClientCapabilities, GotoCapability};
        match self.client_capabilities.text_document {
            Some(TextDocumentClientCapabilities {
                definition: Some(GotoCapability {
                    link_support: Some(true), ..
                }), ..
            }) => Some(GotoDefinitionResponse::Link(vec![LocationLink {
                target_uri: uri,
                origin_selection_range: Some(utils::span_to_range(src, ref_ident.span())),
                target_range: pos,
                target_selection_range: utils::span_to_range(src, object.id.as_ref().unwrap().span())
            }])),
            _ => Some(GotoDefinitionResponse::Array(vec![Location {
                uri,
                range: pos
            }]))
        }
    }

    #[inline(always)]
    #[method(req GotoDeclaration)]
    #[capability("declarationProvider": true)]
    pub fn goto_declaration(&self, params: Method::Params) -> Method::Result {
        self.goto_definition(params)
    }


    fn send_diagnostics(&self, document_url: Url, document_version: i32) {
        let document = match self.documents.get(&document_url) {
            Some(document) => document,
            None => return // Err(ErrorKind::DocumentNotFound)
        };

        self.send(Message::Notification(Notification::new(
            PublishDiagnostics::METHOD.into(),
            PublishDiagnosticsParams {
                uri: document_url,
                diagnostics: Errors(&document.1.errors).into_diagnostics(document.0.as_str()),
                version: Some(document_version)
            }
        )))
    }
}