mod tokens;
mod errors;
mod syntax;
pub mod utils;
mod lsp;

use syntax::BlueprintSyntax;
use errors::Errors;
use std::{io, rc::Rc, path::PathBuf, sync::Mutex};
use lsp_server::Message;

pub static PRINT_COLOR: Mutex<bool> = Mutex::new(true);


pub fn compile_blp(file_path: PathBuf, src: String, compressed: bool) -> io::Result<String> {
    let src = Rc::new(src);
    let blp = BlueprintSyntax::new(src.clone());
    // TODO: validate syntax

    if !blp.errors.is_empty() {
        return Err(io::Error::new(
            io::ErrorKind::Other,
            Errors(blp.errors).display(&file_path, src.as_str())
        ))
    }

    todo!()
}

pub fn language_server(log_path: Option<PathBuf>) -> io::Result<()> {
    let mut server = lsp::Server::new(log_path)?;
    
    while let Some(msg) = server.receive() {
        match msg {
            Message::Request(request) => {
                match server.handle_shutdown(&request) {
                    Ok(true) => break,
                    Ok(false) => {},
                    Err(error) => return Err(server.error(error.to_string()))
                };
                server.log(format!("Receive Request: {}", serde_json::to_value(&request).unwrap()));
                server.handle_request(request)?;
            },
            Message::Notification(notif) => {
                server.log(format!("Receive Notification: {}", serde_json::to_value(&notif).unwrap()));
                server.handle_notification(notif)?;
            },
            Message::Response(response) => {
                server.log(format!("Receive Response: {}", serde_json::to_value(&response).unwrap()));
                // Server does not send requests, so it should not be getting responses
            }
        }
    }

    server.log("--- Shutdown Server ---");
    Ok(())
}