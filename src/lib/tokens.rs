use std::fmt::Display;
use std::rc::Rc;

use blueprint_derive::{Token, SyntaxNode, Parse};

use crate::errors::{Error, ErrorKind};
use crate::syntax::{Parse, ParseContext, SyntaxNode};
use crate::utils::{Result, Span};

const INLINE_COMMENT_OPENING: &str = "//";
const INLINE_COMMENT_CLOSING: &str = "\n";
const MULTILINE_COMMENT_OPENING: &str = "/*";
const MULTILINE_COMMENT_CLOSING: &str = "*/";


/// Iterates over the [`Token`]s created from parsing source code. [`Token`]s are
/// generated lazily, as [`Self::next`] is called. Calling [`Self::next`] could give
/// an [`Error`] which should be pushed to the context's list of errors.
#[derive(Debug, Clone, PartialEq)]
pub struct TokenStream {
    src: Rc<String>,
    // Real position of the TokenStream relative to the start of the source code.
    pos: usize,
    /// The span of the token that was just parsed. Use `prev_span.end` as the
    /// mutable pos that the functions in [`parse`] take as an argument, and
    /// `prev_span.start` as the start_pos variable.
    prev_span: Span
}
impl TokenStream {
    pub fn new(src: Rc<String>) -> Self {
        Self { src, pos: 0, prev_span: 0..0 }
    }
    pub fn skip_space(&mut self) -> Result<()> {
        self.pos = parse::skip_whitespace_and_comments(self.src.clone(), self.pos)?;
        Ok(())
    }
    
    #[inline]
    pub fn remaining_src(&self) -> &str {
        &self.src[self.pos..]
    }
    #[inline]
    pub fn pos(&self) -> usize {
        self.pos
    }
    #[inline]
    pub fn span(&self) -> &Span {
        &self.prev_span
    }
}
impl Iterator for TokenStream {
    type Item = Result<TokenTree>;

    fn next(&mut self) -> Option<Self::Item> {
        /* How it works: Call a function that tries to parse the source string into
           a `Token`. If the funciton determines that it is the *kind* of *token* it
           is looking for, or if there was an *error* parsing the code, return it
           and move on to the source code after that `Token` (this is why on call
           that returns `Some` the *src* variable is changed to one of the returns
           of the parser functions). */
        if let Err(error) = self.skip_space() {
            return Some(Err(error))
        }
        if self.remaining_src().is_empty() {
            return None
        }

        // Start from the point after the whitespace or comment
        let start = self.pos;

        for parser in parse::PARSERS {
            match parser(self.src.clone(), &mut self.pos) {
                Ok(Some(token)) => {
                    self.prev_span.start = start;
                    self.prev_span.end = self.pos;
                    return Some(Ok(token));
                },
                Ok(None) => {},
                Err(error) => {
                    self.prev_span.start = start;
                    self.prev_span.end = self.pos;
                    return Some(Err(error))
                }
            }
        }

        None
    }
}


#[derive(Debug, PartialEq)]
pub enum TokenTree {
    StrLit(StrLit),
    NumLit(NumLit),
    Punct(Punct),
    Ident(Ident),
}
#[derive(Debug, Clone, PartialEq)]
pub enum TokenKind {
    StrLit,
    NumLit,
    Punct,
    Ident
}
impl SyntaxNode for TokenTree {
    fn span(&self) -> Span {
        match self {
            Self::StrLit(token) => token.span(),
            Self::NumLit(token) => token.span(),
            Self::Punct(token) => token.span(),
            Self::Ident(token) => token.span(),
        }
    }
}
impl Display for TokenKind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            Self::StrLit => "String Literal",
            Self::Punct => "Punctuation",
            Self::NumLit => "Number Literal",
            Self::Ident => "Identifier",
        })
    }
}


/// Meant to be only used with derive macro. Name of the struct must match a TokenKind.
pub trait Token: Parse + SyntaxNode {
    /// Get the substring of the `Token` as it appears in teh source code.
    fn slice(&self) -> &str;
    /// The corresponding variant of [`TokenKind`].
    fn kind() -> TokenKind;
}


#[derive(Token, SyntaxNode, Parse, Debug, PartialEq)]
pub struct StrLit {
    src: Rc<String>,
    span: Span
}
// impl<'a> StrLit<'a> {
//     /// Get the content of the String Literal, with the quotes removed. Escape
//     /// characters (prefixed with `\`) are not resolved.
//     #[inline]
//     pub fn value(&self) -> &'a str {
//         &self.slice[1..self.slice.len() - 1]
//     }
// }

#[derive(Token, SyntaxNode, Parse, Debug, PartialEq)]
pub struct NumLit {
    pub ty: NumType,
    src: Rc<String>,
    span: Span,
}
#[derive(Debug, PartialEq)]
pub enum NumType {
    Int, Float
}

/// Punctuation tokens only have 1 character in their slice
#[derive(Token, SyntaxNode, Parse, Debug, PartialEq)]
pub struct Punct {
    src: Rc<String>,
    span: Span,
}

#[derive(Token, SyntaxNode, Parse, Debug, PartialEq)]
pub struct Ident {
    src: Rc<String>,
    span: Span,
}


/// A very useful function to parse any general Token. Used by the [`Parse`] derive macro.
fn parse_token<T: Token>(
    ctx: &mut ParseContext,
    func: fn(Rc<String>, &mut usize) -> Result<Option<T>>
) -> Result<Option<T>>
{
    // Remove whitespace. Techincally this advances TokenStream, but not really
    // because whitespace and comments are not tokens, so all the tokens will be
    // preserved. The purpose of doing this is that if the next token isn't of
    // this type, the next time a parse occurs it won't have to parse through
    // the same whitespace again.
    ctx.tokens.skip_space()?;

    if ctx.tokens.remaining_src().is_empty() {
        return Ok(None)
    }

    if let Some(token) = func(ctx.tokens.src.clone(), &mut ctx.tokens.pos)? {
        // Only advance TokenStream if is Some(T)
        ctx.tokens.prev_span = token.span();
        Ok(Some(token))
    } else {
        Ok(None)
    }
}


/// Module that holds the parser functions.
/// 
/// Before calling one of the parser functions (except whitespace), check if **src** is empty.
/// Empty **src** can cause overflows in the [`TokenStream`] position.
mod parse {
    use super::{Rc, Result, Error, ErrorKind, NumType,
        INLINE_COMMENT_OPENING, INLINE_COMMENT_CLOSING,
        MULTILINE_COMMENT_OPENING, MULTILINE_COMMENT_CLOSING, TokenTree
    };
    /// Used to get the next token in the `TokenStream`, but when the concrete type
    /// is unknown, it must be wrapped in an enum.
    pub const PARSERS: &[fn(Rc<String>, &mut usize) -> Result<Option<TokenTree>>] = &[
        // Punct must go after string_lit, and ident last
        /* StrLit */ |src, pos| {
            match StrLit(src, pos)? {
                Some(token) => Ok(Some(TokenTree::StrLit(token))),
                None => Ok(None)
            }
        },
        /* Punct */ |src, pos| {
            match Punct(src, pos)? {
                Some(token) => Ok(Some(TokenTree::Punct(token))),
                None => Ok(None)
            }
        },
        /* NumLit */ |src, pos| {
            match NumLit(src, pos)? {
                Some(token) => Ok(Some(TokenTree::NumLit(token))),
                None => Ok(None)
            }
        },
        /* Ident */ |src, pos| {
            match Ident(src, pos)? {
                Some(token) => Ok(Some(TokenTree::Ident(token))),
                None => Ok(None)
            }
        },
    ];


    /// Returns the new **pos**ition of the [`TokenStream`] after the parsed whitespace.
    /// 
    /// Can fail with [`ErrorKind::UnclosedDelimiter`] if a multi-line comment was
    /// not closed with a `*/`.
    /// 
    /// [TokenStream]: super::TokenStream
    pub fn skip_whitespace_and_comments(src: Rc<String>, mut pos: usize) -> Result<usize> {
        let mut slice = &src[pos..];
        loop {
            if slice.starts_with(char::is_whitespace) {
                slice = {
                    let trimmed = slice.trim_start();
                    pos += slice.len() - trimmed.len();
                    trimmed
                };

                continue
            } else if  slice.starts_with(INLINE_COMMENT_OPENING) {
                // Inline comment ends with `\n`
                match slice.split_once(INLINE_COMMENT_CLOSING) {
                    Some((comment, rest)) => {
                        // Include the '\n' in the comment because it is skipped
                        pos += comment.len() + INLINE_COMMENT_CLOSING.len();
                        slice = rest;
                    },
                    // The rest of the source is the comment
                    None => {
                        pos += slice.len();
                        slice = "";
                    }
                }
                
                continue
            } else if slice.starts_with(MULTILINE_COMMENT_OPENING) {
                // Multiline comment ends with `*/`
                match slice.split_once(MULTILINE_COMMENT_CLOSING) {
                    Some((comment, rest)) => {
                        // Include the closing `*/` in the comment
                        pos += comment.len() + MULTILINE_COMMENT_CLOSING.len();
                        slice = rest;
                    },
                    // Did not find closing `*/`
                    None => {
                        let start_pos = pos;
                        pos += slice.len();
                        return Err(Error(
                            ErrorKind::UnclosedDelimiter("*/"),
                            start_pos..pos
                        ))
                    }
                }

                continue
            }
            break
        }

        Ok(pos)
    }

    #[allow(non_snake_case)]
    /// Gives you a String Literal if the first character of **src** at **pos** is `"` or `'`.
    /// 
    /// Can fail with [`ErrorKind::UnclosedDelimiter`] if closing quote was not
    /// found. Can also fail with [`ErrorKind::StrNewLine`] if string contains a
    /// **new-line** `\n` char.
    pub fn StrLit(src: Rc<String>, pos: &mut usize) -> Result<Option<super::StrLit>> {
        let slice = &src[*pos..];

        // String Literal is delimited by `"`
        if slice.starts_with(|c| c == '"' || c == '\'') {
            let start_pos = *pos;
            let mut iter = slice.as_bytes().iter();
            let mut has_new_line = false;
            // single- or double-quote
            let quote = *iter.next().unwrap();
            *pos += 1; // Increment for the opening quote
            
            while let Some(c) = iter.next() {
                *pos += 1;

                match c {
                    // Escaped Quote (or any other char)
                    b'\\' => if iter.next().is_some() {
                        *pos += 1;
                    },
                    // Closing Quote. Single- or double-quote
                    c if *c == quote => return if has_new_line {
                        Err(Error(ErrorKind::StrNewLine, start_pos..*pos))
                    } else {
                        Ok(Some(super::StrLit {
                            src,
                            span: start_pos..*pos
                        }))
                    },
                    // Having a linebreak in a string is not allowed, but still try
                    // to reach the closing quote so that the rest of the parsing
                    // can cintinue normally.
                    b'\n' => has_new_line = true,
                    _ => {}
                }
            }

            // Could not find closing quote
            Err(Error(ErrorKind::UnclosedDelimiter("\""), start_pos..*pos))
        } else {
            Ok(None)
        }
    }

    #[allow(non_snake_case)]
    /// Each punctuatution is returned one at a time. E.g. if `src="#$%"`, then this
    /// function will return `Token` with *slice* "#", and when called again it will
    /// return `Token` with *slice* "#", and so on.
    pub fn Punct(src: Rc<String>, pos: &mut usize) -> Result<Option<super::Punct>> {
        if src[*pos..].starts_with(|first: char|
            first.is_ascii_punctuation() && first != '_'
        ) {
            let token = super::Punct {
                src,
                span: *pos..(*pos + 1)
            };
            *pos += 1;

            Ok(Some(token))
        } else {
            Ok(None)
        }
    }

    #[allow(non_snake_case)]
    /// Number Literals can have *Decimal* digits **0 - 9**. If the lit is prefixed
    /// with `0x`, it can have *Hexadecimal* digits **0 - F**. And if it is prefixed
    /// with `0b`, it can have *Binary* digits **0 - 1**.
    /// 
    /// Underscores (_) can be used as visual separators. So the following are valid
    /// IntLit Tokens: `123_456_789`, `0xABC_EFG`
    /// 
    /// Returned `Token` can be an *Integer* or *Float*. Can fail with
    /// [`ErrorKind::InvalidDigits`] if its digits fall outside the defined ranges.
    pub fn NumLit(src: Rc<String>, pos: &mut usize) -> Result<Option<super::NumLit>> {
        let slice = &src[*pos..];

        // Ensure it is at least a decimal digit to proceed
        if slice.starts_with(|c: char| c.is_digit(10)) {
            let start_pos = *pos;
            // Is `None` when there is no error
            let mut error_start: Option<usize> = None;
            let mut base = 10;
            let mut iter = slice.chars();
            let mut has_point = false;

            // See if Literal is a *hexadecimal* or *binary*. If it is either, only
            // the part after needs to be parsed, so advance iter by 2
            if slice.strip_prefix("0x").is_some() {
                base = 16;
                iter.next(); iter.next(); // ? iter.advance_by(2); unstable
                *pos += 2;
            } else if slice.strip_prefix("0b").is_some() {
                base = 2;
                iter.next(); iter.next(); // ? iter.advance_by(2); unstable
                *pos += 2;
            }
           
            while let Some(c) = iter.next() {
                if c.is_alphanumeric() && !c.is_digit(base) {
                    // Example: number literal is base 10, but found digit `A`
                    if error_start.is_none() {
                        error_start = Some(*pos);
                    }
                }
                // Decimal point for FloatLit Token
                else if !has_point && c == '.' {
                    has_point = true;
                }
                // char is Punctuation, or whitespace, etc.
                else if (c.is_ascii_punctuation() && c != '_')
                || c.is_whitespace() {
                    break
                }
                
                *pos += c.len_utf8();
            }

            // No more characters
            return if let Some(start) = error_start {
                Err(Error(
                    ErrorKind::InvalidDigits(match base {
                        2 => '1', 10 => '9', 16 => 'F',
                        base => panic!("IntLit can't have base {base}. This branch should be impossible")
                    }),
                    (start..*pos).into()
                ))
            } else {
                Ok(Some(super::NumLit {
                    ty: if has_point {
                        NumType::Float
                    } else {
                        NumType::Int
                    },
                    src,
                    span: start_pos..*pos
                }))
            }
        }

        Ok(None)
    }

    #[allow(non_snake_case)]
    /// Anything else that is not a `StrLit`, `Punct`, `Number` or `Whitespace`. Can
    /// contain *underscores* (_), *hyphens* (-), and *digits*, but can't start
    /// with a digit or hyphen.
    pub fn Ident(src: Rc<String>, pos: &mut usize) -> Result<Option<super::Ident>> {
        let mut slice = &src[*pos..];
        
        slice = match slice.split_once(|c: char| 
            (c.is_ascii_punctuation() && c != '_' && c != '-') || c.is_whitespace()
        ) {
            Some((ident, _)) => ident,
            None => slice
        };

        if let Some(first) = slice.chars().nth(0) {
            if first != '-' && !first.is_ascii_digit() {
                let start = *pos;
                *pos += slice.len();
                return Ok(Some(super::Ident {
                    src,
                    span: start..*pos
                }))
            }
        }

        Ok(None)
    }
}