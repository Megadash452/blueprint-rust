use std::{path::PathBuf, fmt::Display};
use lsp_types::Diagnostic;
use crate::utils::{self, colors, Span, Position};
use crate::tokens::TokenKind;


#[derive(Clone)]
pub struct Error(pub ErrorKind, pub Span);
impl Error {
    pub fn display(&self, file_path: &PathBuf, src: &str) -> String {
        #[allow(non_snake_case)]
        let (ERROR_COL, HILIGHT_COL, RESET_COL) = if *crate::PRINT_COLOR.lock().unwrap() {
            (colors::ERROR, colors::HILIGHT, colors::RESET)
        } else {
            ("", "", "")
        };

        // The section must be in the Lines format
        let section = utils::span_to_range(src, self.1.clone());

        format!("Compiler Error: {ERROR_COL}{msg}{RESET_COL}\n\
                 At {HILIGHT_COL}{file_path:?}{RESET_COL}::{HILIGHT_COL}{range}{RESET_COL}:\n\
                 {snippet}", // snippet ends with '\n'
            msg = self.0,
            snippet = utils::code_snippet(section, src),
            // Don't write the range next to the file if it's all 1
            range = if !(section.start.line == 1 && section.end.character - section.start.character == 0) {
                utils::display_range(section)
            } else {
                String::new()
            }
        )
    }

    pub fn into_diagnostic(&self, src: &str) -> Diagnostic {
        Diagnostic {
            range: utils::span_to_range(src, self.1.clone()),
            severity: Some(lsp_types::DiagnosticSeverity::ERROR),
            code: None,
            code_description: None,
            source: Some("Blueprint".to_owned()),
            message: self.0.to_string(),
            related_information: None,
            tags: None,
            data: None
        }
    }
}


/// Wrapper type for error slices.
pub struct Errors<T: AsRef<[Error]>>(pub T);
impl<T: AsRef<[Error]>> Errors<T> {
    pub fn display(self, file_path: &PathBuf, src: &str) -> String {
        let slice = self.0.as_ref();
        let mut buf = format!("Encountered {} errors:\n", slice.len());

        slice.iter()
            .map(|error|
                error.display(&file_path, src)
            ).for_each(|s|
                buf.push_str(&s)
            );

        buf
    }
    pub fn into_diagnostics(self, src: &str) -> Vec<Diagnostic> {
        self.0.as_ref().iter()
            .map(|error| error.into_diagnostic(src))
            .collect()
    }
}


#[derive(Debug, Clone, PartialEq)]
pub enum ErrorKind {
    // EmptyFile,
    // UsingGtkNotFound,
    /// Syntax Parser expected a `TokenType` with *(optionally)* a specific value.
    Expected(TokenKind, Option<String>),
    UnexpectedTokens,
    // UnexpectedValue,

    /// Contains expected delimiter. E.g. An unclosed multiline commment expects
    /// `*/` as a closing delimiter.
    UnclosedDelimiter(&'static str),
    StrNewLine,
    /// Contained char is the highest digit the number can have.
    InvalidDigits(char),
    // UnknownObject,
    // UnknownProperty,
    /// Contains Separator's slice
    ConsecutiveSep(&'static str),
    /// An [`Object`] is trying to use an ID that is already being used by another [`Object`].
    /// Stores the ID and the [`Position`] of the other object with that ID.
    UsedId(String, Position),

    Custom(String)
}
impl Display for ErrorKind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Expected(_, Some(value)) => write!(f, "Expected {value}"),
            Self::Expected(TokenKind::Ident, _) => write!(f, "Expected an {}", TokenKind::Ident),
            Self::Expected(kind, _) => write!(f, "Expected a {kind}"),
            
            Self::UnexpectedTokens => write!(f, "Unexpected Tokens"),
            Self::UnclosedDelimiter(delim) => write!(f, "Unclosed Delimiter. Expected {delim}"),
            Self::StrNewLine => write!(f, "Strings can't contain line-breaks"),
            Self::ConsecutiveSep(sep) => write!(f, "Expected item. Can't have consecutive {} separators", sep),
            Self::UsedId(id, pos) => write!(f, "ID \"{id}\" is already in use at {}.", utils::display_pos(*pos)),

            Self::InvalidDigits('1') => write!(f, "Binary numbers can only have digits 0 and 1"),
            Self::InvalidDigits('9') => write!(f, "Decimal numbers can only have digits 0 to 9"),
            Self::InvalidDigits('F') => write!(f, "Hexadecimal numbers can only have digits 0 to F"),
            Self::InvalidDigits(max_digit) => write!(f, "This type of number can only have digits 0 to {max_digit}"),

            Self::Custom(msg) => write!(f, "{msg}")
        }
    }
}
