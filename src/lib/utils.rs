use crate::errors::Error;
use std::fmt::Write;

pub type Span = std::ops::Range<usize>;
/// The **line** position is inclusive, but the **character** position is exclusive.
/// Comparable to **lines**: `0..=0`, **columns**: `0..0`. 
pub type LinesRange = lsp_types::Range;
pub use lsp_types::Position;
pub type Result<T> = std::result::Result<T, Error>;


pub mod colors {
    pub static ERROR: &str = "\x1B[1;91m";
    pub static HILIGHT: &str = "\x1B[1;97m";
    pub static RESET: &str = "\x1B[00m";
    pub static LINE_NUM: &str = "\x1B[2;97m";
    pub static BAR: &str = "\x1B[1;90m";
}


pub fn digits(mut num: usize, base: u32) -> u32
// where N: PartialOrd<u32> + DivAssign<u32>
{
    let mut digits = 0;

    while num > 0 {
        digits += 1;
        num /= base as usize;
    }
    digits
}

/// Generate a code snippet for the user to view. ***src*** is the original source code
pub fn code_snippet(section: LinesRange, src: &str) -> String {
    #[allow(non_snake_case)]
    let (LINE_NUM_COL, BAR_COL, RESET_COL) = if *crate::PRINT_COLOR.lock().unwrap() {
        (colors::LINE_NUM, colors::BAR, colors::RESET)
    } else {
        ("", "", "")
    };
    /* How frequent a line number is printed while writing code_snippets. E.g. every
       5 lines. */
    const PRNT_LN_FREQ: u32 = 5;
    // The maximum number of lines the error can print
    const PRNT_LN_MAX: u32 = 10;

    let mut buf = String::new();
    // How many columns are needed to print the line numbers
    let line_space = digits((section.end.line + 1) as usize, 10) as usize;

    // Iterate over the lines of source code that fall within the given *section*
    for (line_num, line) in IterLines::new(src)
        .enumerate()
        .filter(|(line_num, _)|
            *line_num >= section.start.line as usize
            && *line_num <= section.end.line as usize
        )
    {
        // Stop printing code snippet after 10 lines were printed
        if line_num - section.start.line as usize >= PRNT_LN_MAX as usize {
            break
        }

        // Line number is aligned right
        writeln!(buf, "{LINE_NUM_COL} {:>line_space$}{BAR_COL} | {RESET_COL}{line}",
            // Every 5 lines the line number will be written
            if (line_num - section.start.line as usize) % PRNT_LN_FREQ as usize == 0 {
                (line_num + 1).to_string()
            } else {
                String::new()
            }
        ).unwrap();
        // TODO: print ^ underline regardless of if it is 1 line
    }

    // Check if is one line to underline it with ^ chars
    if section.end.line - section.start.line == 0 {
        // how many characters are before the error starts
        let pre_len = section.start.character as usize;
        let err_len = (section.end.character - section.start.character) as usize;
        // 0 (""): The emtpy str argument allows for format repetition (as a hack)
        // 1 ('^'): Prints at least 1 '^', even if *column* range is empty (err_len == 0).
        writeln!(buf, "{LINE_NUM_COL} {0:line_space$}   {0:pre_len$}{1:^>err_len$}{RESET_COL}", "", '^').unwrap();
    }

    buf
}


pub use lsp_conversions::*;
/// These functions help convert between positional structs of the Language's internals and [`lsp_types`].
mod lsp_conversions {
    use super::{Span, LinesRange, IterLines, Position};

    /// Get line/column position of a *byte* index in the **source** code.
    pub fn index_to_pos(src: &str, index: usize) -> Position {
        let pos = Position { line: 0, character: 0 };
        // The sum of the length of accumulated lines during iteration, including the '\n'
        let mut i = 0;
    
        for (line_num, line) in IterLines::new(src).enumerate() {
            i += line.len();
    
            if i >= index {
                return Position {
                    line: line_num as u32,
                    character: {
                        // i >= src_range.start
                        // i - src_range.start <= line.len()
                        // Therefore, this subtraction should not overflow
                        let last = line.len() - (i - index);
                        line[..last].chars().count() as u32
                    }
                }
            }
    
            // Include the '\n' for the next iteration.
            i += 1;
        }
    
        // occurs if index > src.len()
        panic!("Position line <{}> overflow", pos.line)
    }
    
    /// Get the *byte* offset of a character at a certain **pos**ition in the **source** code.
    pub fn pos_to_index(src: &str, pos: Position) -> usize {
        // The sum of the length of accumulated lines during iteration, including the '\n'
        let mut index = 0;
    
        for (line_num, line) in IterLines::new(src).enumerate() {
            if line_num == pos.line as usize {
                return index + column_length(line, pos.character)
            }
    
            index += line.len() + 1/* Include the '\n' */;
        }
    
        panic!("Position line <{}> overflow", pos.line)
    }
    
    /// Get a [`Range`] of **lines** and **columns** from a span.
    /// 
    /// For this to work, all of the original source code must be passed to the
    /// function. The section of the source code is defined by the *span*.
    /// Span is measured in bytes, but [`LinesRange`] is measured in chars.
    /// 
    /// [Range]: LinesRange
    pub fn span_to_range(src: &str, span: Span) -> LinesRange {
        let mut start = Position { line: 0, character: 0 };
        let mut finished_start = false;
        // The sum of the length of accumulated lines during iteration, including the '\n'
        let mut i = 0;
    
        // -- CHECKS
        // Range must be forward
        assert!(span.start <= span.end, "src_range ({:?}) must be a Forward Range", span);
    
        // Look for the line the range starts at and ends at
        for (line_num, line) in IterLines::new(src).enumerate() {
            // These are measured in bytes to find the line src_range.start is in,
            // because src_range is measured in bytes
            let line_len = line.len();
            i += line_len;
    
            // Set what lines the range starts and ends at if they haven't been set
            if !finished_start && i >= span.start {
                start.line = line_num as u32;
                // i >= src_range.start
                // i - src_range.start <= line.len()
                // Therefore, this subtraction should not overflow
                let last = line_len - (i - span.start);
                start.character = line[..last].chars().count() as u32;
                finished_start = true;
            }
            if i >= span.end {
                return LinesRange {
                    start,
                    end: Position {
                        line: line_num as u32,
                        character: {
                            // i >= src_range.end
                            // i - src_range.end <= line.len()
                            // Therefore, this subtraction should not overflow
                            let last = line_len - (i - span.end);
                            line[..last].chars().count() as u32
                        }
                    }
                }
            }
    
            // Include the '\n' for the next iteration.
            i += 1;
        }
    
        panic!("src_range ({:?}) must not pass src length ({})", span, src.len());
    }
    
    /// Will panic if **lines** or **columns** go beyond the end of the src (aka it overflows).
    pub fn range_to_span(src: &str, range: LinesRange) -> Span {
        // start and end measured in bytes
        let mut start = 0;
        // The sum of the length of accumulated lines during iteration, including the '\n'
        let mut i = 0;
    
        for (line_num, line) in IterLines::new(src).enumerate() {
            if line_num == range.start.line as usize {
                start = i + column_length(line, range.start.character);
            }
            if line_num == range.end.line as usize {
                return start..(i + column_length(line, range.end.character))
            }
    
            i += line.len() + 1/* Include the '\n' */;
        }
    
        panic!("LinesRange line overflow")
    }
    
    
    /// Tells the byte offset of a **column** (0-based).
    /// Panics if the column does not exist in this line.
    fn column_length(line: &str, target_col: u32) -> usize {
        // measured in bytes
        let mut buf = 0;
        // measured in codepoints
        let mut chars_count = 0;
        for ch in line.chars().take(target_col as usize) {
            chars_count += 1;
            buf += ch.len_utf8();
        }
        if chars_count < target_col {
            // the actual number of chars in the line is less than the column
            panic!("Position column <{target_col}> overflow")
        }
    
        buf
    }
}


/// Could print the LinesRange in different ways:
///  1. `lin_start:col_start..lin_end:col_end` When there are multiple lines.
///  2. `lin:col_start..col_end` When there is only one line.
///  3. `lin:col` When there is only one line and no more than 1 column.
pub fn display_range(range: LinesRange) -> String {
    let mut buf = String::new();
    /* LinesRange is 0-based, so add 1 to all its fields
       so that the user sees it as it is in the editor,
       where lines and columns are 1-based */
    let line = (range.start.line + 1)..(range.end.line + 1);
    let col = (range.start.character + 1)..(range.end.character + 1);

    if line.len() > 0 {
        // multiple lines
        buf.push_str(&format!("{}:{}..{}:{}", line.start, col.start, line.end, col.end))
    } else if col.len() > 1 {
        // only 1 line
        buf.push_str(&format!("{}:{}..{}", line.start, col.start, col.end))
    } else {
        // only 1 line && less than 2 columns
        buf.push_str(&format!("{}:{}", line.start, col.start))
    }

    buf
}
pub fn display_pos(pos: Position) -> String {
    format!("{}:{}", pos.line, pos.character)
}


/// Merges [`Error`]s that are the same [`ErrorKind`] and their [`Span`] intersect.
/// Imagine like one of the errors starts inside the previous one,
/// and the previous one ends inside that error.
/// If 2 consecutive [`Error`]s are only separated by whitespace, they will also be merged.
/// 
/// This function expects that the errors are ***in order*** by [`Span`].
pub(super) fn merge_consecutive_errors(errors: Vec<Error>, src: &str) -> Vec<Error> {
    let mut buf: Vec<Error> = vec![];
    let mut prev_err: Option<&mut Error> = None;

    for error in errors.into_iter() {
        match prev_err {
            // Have same ErrorKind and overlapping spans
            Some(prev)
                if prev.0 == error.0
                && prev.1.end >= error.1.start => {
                    /* In case that the current error is containsed inside the previous error,
                    the previous error remains unchanged, and the current error is completely discarded */
                    if prev.1.end <= error.1.end {
                        // extend previous error, and discard the current one
                        prev.1.end = error.1.end
                    }
                    prev_err = Some(prev);
                },
            // Have same ErrorKind, but are separated by whitespace
            Some(prev)
                if prev.0 == error.0
                && src[prev.1.end..error.1.start].chars().all(|c| c.is_whitespace()) => {
                    // extend previous error, and discard the current one
                    prev.1.end = error.1.end;
                    prev_err = Some(prev);
                },
            _ => {
                buf.push(error);
                prev_err = Some(buf.last_mut().unwrap())
            }
        }
    }

    buf
}


#[derive(Clone)]
/// Iterate over the lines of a string. Unlike
/// [`str::lines`](https://doc.rust-lang.org/std/primitive.str.html#method.lines),
/// this one returns the last line even if it's empty.
/// 
/// ```
/// use blueprint::utils::IterLines;
/// // src has 4 lines
/// let src = "line1\nline2\nline3\n";
/// let mut iter = IterLines::new(src);
/// 
/// assert_eq!(iter.next(), Some("line1"));
/// assert_eq!(iter.next(), Some("line2"));
/// assert_eq!(iter.next(), Some("line3"));
/// assert_eq!(iter.next(), Some("")); // line 4
/// assert_eq!(iter.next(), None);
/// ```
pub struct IterLines<'a> {
    slice: &'a str,
    finished: bool
}
impl<'a> IterLines<'a> {
    pub fn new(slice: &'a str) -> Self {
        Self { slice, finished: false }
    }
}
impl<'a> Iterator for IterLines<'a> {
    type Item = &'a str;
    
    fn next(&mut self) -> Option<Self::Item> {
        if self.finished {
            return None
        }

        match self.slice.split_once('\n') {
            Some((line, rest)) => {
                self.slice = rest;
                Some(line)
            },
            None => {
                self.finished = true;
                Some(self.slice)
            }
        }
    }
}
