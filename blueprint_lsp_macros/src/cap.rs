use quote::{
    // From proc-macro2
    __private::{TokenStream as TokenStream2, Punct, Spacing, Group, Delimiter},
    ToTokens, TokenStreamExt
};
use syn::{LitStr, parse::{Parse, ParseStream}, Token, punctuated::Punctuated};
use std::collections::HashMap as Map;

#[derive(Debug)]
pub enum Value {
    Object(Map<String, Value>),
    Other(TokenStream2)
}
impl Value {
    /// Recursively merge object values.
    /// 
    /// Merge two JSON [`Value`]s using the following critera:
    ///  - `Object`: Looks for matching keys,
    ///                           and applies this function recursively to both keys' [`Values`].
    ///  - `Other`: **Conflicting Error** if values don't have the same Tokens.
    pub fn merge_with(&mut self, other: Self) -> Result<(), String> {
        match (self, other) {
            (Self::Object(map1), Self::Object(map2)) => {
                for (key, val2) in map2 {
                    match map1.get_mut(&key) {
                        // entry in obj1 is merged with the same entry from obj2
                        Some(val1) => val1.merge_with(val2)?,
                        // entries not in obj1 are inserted
                        None => { map1.insert(key, val2); }
                    }
                }
    
                Ok(())
            },
            (val1, val2) => if *val1 == val2 {
                    Ok(())
                } else {
                    Err("Conflicting Values".to_string())
                }
        }
    }
}
impl PartialEq for Value {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Object(map1), Self::Object(map2)) =>
                map1 == map2,
            (Self::Other(tokens1), Self::Other(tokens2)) =>
                tokens1.to_string() == tokens2.to_string(),
            _ => false
        }
    }
}
impl Parse for Value {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        match syn::__private::parse_braces(&input) {
            Ok(braces) => {
                let pairs: Punctuated::<Pair, Token![,]> = braces.content.parse_terminated(Pair::parse)?;
                let mut map: Map<String, Value> = Map::new();

                for Pair { key, value } in pairs {
                    map.insert(key, value);
                }
                
                Ok(Self::Object(map))
            },
            Err(_) => Ok(Self::Other(input.parse::<TokenStream2>()?))
        }
    }
}
impl ToTokens for Value {
    fn to_tokens(&self, tokens: &mut TokenStream2) {
        match self {
            Self::Object(map) => {
                let mut stream_buf = TokenStream2::new();
                for (key, value) in map {
                    key.to_tokens(&mut stream_buf);
                    stream_buf.append(Punct::new(':', Spacing::Alone));
                    value.to_tokens(&mut stream_buf);
                    stream_buf.append(Punct::new(',', Spacing::Alone));
                }
                // Tokens are enclosed in braces `{...}`
                tokens.append(Group::new(Delimiter::Brace, stream_buf))
            },
            Self::Other(val) => tokens.append_all(val.clone())
        }
    }
}

pub struct Pair {
    pub key: String,
    pub value: Value,
}
impl Parse for Pair {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        let key = input.parse::<LitStr>()?.value();
        input.parse::<Token![:]>()?;
        let value = input.parse()?;

        Ok(Self { key, value })
    }
}