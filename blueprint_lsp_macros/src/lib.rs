use std::collections::HashMap;
use proc_macro::TokenStream;
use syn::{ItemFn, ImplItem, spanned::Spanned, Ident, Path, Stmt};
use quote::{
    // From proc-macro2
    __private::TokenStream as TokenStream2,
    quote, quote_spanned, ToTokens
};

mod method;
mod cap;

use method::Method;
use cap::Value;


#[proc_macro_attribute]
#[allow(non_snake_case)]
/// Helps create things like the server capabilities
/// by looking at attributes of inner items.
/// Use it on the impl block of a struct.
/// 
/// Has helper attributes `capability` and `method`.
/// 
/// ```no_run
/// use lsp_types::request::GotoDefinition;
/// 
/// struct Server {
///     ...
/// }
/// #[LSP]
/// impl Server {
///     #[method(GotoDefinition)]
///     #[capability("definitionProvider": true)]
///     pub fn goto_definition(&self, params: Method::Params) -> Method::Result {
///         ...
///     }
///     ...
/// }
/// ```
pub fn LSP(attr: TokenStream, input: TokenStream) -> TokenStream {
    let mut imp: syn::ItemImpl = syn::parse(input).unwrap();
    if !attr.is_empty() {
        return quote!{ compile_error!("LSP attribute macro does not take any parameters"); #imp }.into()
    }

    let mut error_buf: Vec::<TokenStream2> = vec![];
    // lsp_type that implements `Request`, and the function in the Server impl
    let mut req_methods: HashMap<String, Ident> = HashMap::new();
    // lsp_type that implements `Notificaiton`, and the function in the Server impl
    let mut notif_methods: HashMap<String, Ident> = HashMap::new();
    // e.g.: Key: "definitionProvider", Value: true
    // Key and value separated by colon
    let mut capabilities: HashMap<String, Value> = HashMap::new();

    for item in &imp.items {
        if let ImplItem::Method(m) = item {
            // Find helper attributes `method` and `capability` and use them
            for attr in m.attrs.iter() {
                match attr.path.segments.last().to_token_stream().to_string().as_str() {
                    "method" => {
                        let (destination, path) = match syn::parse(strip_group(attr.tokens.clone().into())) {
                            Ok(Method::Request(path)) => (&mut req_methods, path.to_token_stream().to_string()),
                            Ok(Method::Notification(path)) => (&mut notif_methods, path.to_token_stream().to_string()),
                            Err(error) => {
                                let error = error.to_string();
                                error_buf.push(quote_spanned!{ attr.tokens.span()=>
                                    compile_error!(#error);
                                });
                                continue
                            }
                        };

                        // Check for duplicate implementation of LSP method
                        if let Some(func_name) = destination.get(&path) {
                            let error = format!("LSP method `{}` has already been implemented in `{}`", path, func_name);
                            error_buf.push(quote_spanned!{ attr.tokens.span()=>
                                compile_error!(#error);
                            });
                            continue
                        }

                        destination.insert(path, m.sig.ident.clone());
                    },
                    "capability" => {
                        let tokens = strip_group(attr.tokens.clone().into());
                        let cap::Pair { key, value } = match syn::parse(tokens) {
                            Ok(cap) => cap,
                            Err(error) => {
                                let error = error.to_string();
                                error_buf.push(quote_spanned!{ attr.tokens.span()=>
                                    compile_error!(#error);
                                });
                                continue
                            }
                        };

                        // If a capability with the same name as this one exists, try merging the values
                        match capabilities.get_mut(&key) {
                            Some(entry_val) => {
                                // Recursively merge object values
                                if let Err(error) = entry_val.merge_with(value) {
                                    error_buf.push(quote_spanned!{ attr.tokens.span()=>
                                        compile_error!(#error);
                                    });
                                    continue
                                }
                            },
                            None => { capabilities.insert(key, value); }
                        }
                    },
                    _ => {}
                }
            }
        }
    }


    // register request methods
    if let Err(error) = prepend_stmts_to_impl_method(
        &mut imp, "handle_request",
        request_stmts(&req_methods)
    ) {
        return error
    }

    // register notification methods
    if let Err(error) = prepend_stmts_to_impl_method(
        &mut imp, "handle_notification",
        notif_stmts(&notif_methods)
    ) {
        return error
    }

    let keys = capabilities.keys();
    let vals = capabilities.values();

    quote! {
        #( #error_buf )*
        #imp
        lazy_static! {
            /// amogus
            pub static ref SERVER_CAPABILITIES: ::lsp_types::ServerCapabilities = ::serde_json::from_value(
                ::serde_json::json!({
                    #( #keys: #vals ),*
                })
            ).expect("JSON Object could not be parsed into `ServerCapabilities`");
        }
    }.into()
}


#[proc_macro_attribute]
/// Takes a path to a struct that implements [`Request`] or [`Notification`]
/// (specify which by using `req` or `notif`).
/// Allows the use of a `Method` as a type shortcut for the
/// struct that was passed in in the arguments and return type.
/// In other words, the symbol `Method` will be replaced with the attribute's input in the function's signature.
/// An LSP method has to take a server as an argument,
/// a mutable or immutable reference (but not move).
/// 
/// When used with the [`LSP`] macro, it will register the method
/// to a function that handles methods of that type.
/// `handle_request` is where [`Request`]s are registered to,
/// and `handle_notification` is where [`Notification`] are registered to.
/// 
///  > `Note`: An method cannot be implemented more than once in an [`LSP`] impl block.
/// 
/// ## Example
/// 

// This method is not part of the server, but takes a server
// 
// ```
// #[method(req lsp_type::request::GotoDefinition)]
// // type Method::Params = GotoDefinition::Params
// // type Method::Params = GotoDefinition::Result
// pub fn goto_definition(server: &Server, params: Method::Params) -> Method::Result {
//     // Result = Option<GotoDefinitionResponse>
//     Some(GotoDefinitionResponse::Array(vec![]))
// }
// ```

/// 
/// This method is part of the Server impl block
/// 
/// ```
/// #[method(req GotoDefinition)]
/// pub fn goto_definition(&self, params: Method::Params) -> Method::Result {
///     Some(GotoDefinitionResponse::Array(vec![]))
/// }
/// ```
/// 
/// [`Request`]: https://docs.rs/lsp-types/0.93.2/lsp_types/request/trait.Request.html
/// [`Notification`]: https://docs.rs/lsp-types/0.93.2/lsp_types/notification/trait.Notification.html
pub fn method(attr: TokenStream, input: TokenStream) -> TokenStream {
    let func: ItemFn = syn::parse(input).unwrap();
    let method_type = match syn::parse::<method::Method>(attr) {
        Ok(path) => path,
        Err(error) => {
            let error = error.to_string();
            return quote!{ compile_error!(#error); }.into()
        }
    };

    method::method(method_type, func)
}


#[proc_macro_attribute]
/// Add a capability to the Server's capabilities.
/// Only works as a helper attribute to the [`LSP`] attribute macro.
/// Otherwise it does nothing.
/// 
/// Takes a JSON Object entry, not a full json object, only the key-value pair.
/// If a capability with that key already exists,
/// then it tries to merge both capabilities `Value`s using the following critera:
///  - *Values* are `Object`:
///  - *Values* are `Array`: Append the Values to the existing array Value.
///  - *Values* are anything else: **Conflicting Error**.
///  - **Mismatch** Value types: **Conflicting Error**.
/// 
/// When used with the [`LSP`] macro, it will push server capabilities to the
/// `SERVER_CAPABILITIES` static.
/// 
/// ## Example
/// 
/// ```no_run
/// #[capability("textDocumentSync": { "openClose": true })]
/// ```
pub fn capability(_attr: TokenStream, input: TokenStream) -> TokenStream {
    // Go to the relevant section of LSP():
    // match attr.path.segments.last().to_token_stream().to_string().as_str() {
    //     ...
    //     "capability" => ...
    //     ...
    // }
    input
}


/// Get rid of outer group tokens (), [], or {}
fn strip_group(input: TokenStream) -> TokenStream {
    let s = input.to_string();

    if let Some(rest) = s.strip_prefix("(") {
        return rest.strip_suffix(")").expect("Expected closing `)`").parse().unwrap()
    }
    if let Some(rest) = s.strip_prefix("[") {
        return rest.strip_suffix("]").expect("Expected closing `]`").parse().unwrap()
    }
    if let Some(rest) = s.strip_prefix("{") {
        return rest.strip_suffix("}").expect("Expected closing `}`").parse().unwrap()
    }
    input
}


/// Push statements to the beginning of a function in an impl block.
/// All the statemetns that were already in the function are pushed to the end.
fn prepend_stmts_to_impl_method(imp: &mut syn::ItemImpl, method_name: impl AsRef<str>, mut stmts: Vec<Stmt>) -> Result<(), TokenStream> {
    match imp.items.iter_mut().find(|item|
        match item {
            ImplItem::Method(m) => m.sig.ident == *method_name.as_ref(),
            _ => false
        } 
    ) {
        Some(ImplItem::Method(method)) => {
            stmts.extend(method.block.stmts.clone());
            method.block.stmts = stmts;
        },
        _ => return Err(quote!{ compile_error!("Server must have function `handle_request`") }.into())
    }

    Ok(())
}

fn request_stmts(map: &HashMap<String, Ident>) -> Vec<Stmt> {
    map.iter().map(|(method_type, func_name)| {
        // Parse path string to a TokenStream so it can be parsed into a `Path`.
        // Never panics because the string was at some point a `Path`.
        let method_type: Path = syn::parse(method_type.parse().unwrap()).unwrap();
        let casted_path = quote! {
            <#method_type as ::lsp_types::request::Request>
        };

        syn::parse::<Stmt>(quote! {
            req = match req.extract::<#casted_path::Params>(#casted_path::METHOD) {
                ::core::result::Result::Ok((id, params)) => {
                    self.send(::lsp_server::Message::Response(::lsp_server::Response {
                        id,
                        result: ::core::option::Option::Some(::serde_json::to_value(
                            self.#func_name(params)
                        )?),
                        error: ::core::option::Option::None
                    }));
                    return ::core::result::Result::Ok(())
                },
                // Release request to try another method
                ::core::result::Result::Err(::lsp_server::ExtractError::MethodMismatch(req)) => req,
                ::core::result::Result::Err(error) => return ::core::result::Result::Err(self.error(error.to_string()))
            };
        }.into()).unwrap()
    }).collect::<Vec<Stmt>>()
}

fn notif_stmts(map: &HashMap<String, Ident>) -> Vec<Stmt> {
    map.iter().map(|(method_type, func_name)| {
        // Parse path string to a TokenStream so it can be parsed into a `Path`.
        // Never panics because the string was at some point a `Path`.
        let method_type: Path = syn::parse(method_type.parse().unwrap()).unwrap();
        let casted_path = quote! {
            <#method_type as ::lsp_types::notification::Notification>
        };

        syn::parse::<Stmt>(quote! {
            notif = match notif.extract::<#casted_path::Params>(#casted_path::METHOD) {
                ::core::result::Result::Ok(params) => return ::core::result::Result::Ok(self.#func_name(params)),
                // Release notification to try another method
                ::core::result::Result::Err(::lsp_server::ExtractError::MethodMismatch(notif)) => notif,
                ::core::result::Result::Err(error) => return ::core::result::Result::Err(self.error(error.to_string()))
            };
        }.into()).unwrap()
    }).collect::<Vec<Stmt>>()
}