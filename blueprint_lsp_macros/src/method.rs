use proc_macro::TokenStream;
use syn::{parse::{Parse, ParseStream}, spanned::Spanned, Ident, FnArg, ItemFn, Path, Type, ReturnType, TypeReference, TypePath, TypePtr};
use quote::{quote, quote_spanned, ToTokens};


pub enum Method {
    Request(Path),
    Notification(Path)
}
impl Method {
    pub fn path(&self) -> &Path {
        match self {
            Method::Request(path) => path,
            Method::Notification(path) => path,
        }
    }
}
impl Parse for Method {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        match input.parse::<Ident>() {
            Ok(ident) if ident.to_string() == "req" => {
                Ok(Self::Request(input.parse::<Path>()?))
            },
            Ok(ident) if ident.to_string() == "notif" => {
                Ok(Self::Notification(input.parse::<Path>()?))
            },
            _ => Err(input.error("Specify `req` or `notif`"))
        }
    }
}


pub fn method(method: Method, mut func: ItemFn) -> TokenStream {
    let method_str = method.path().to_token_stream()
        .to_string()
        .chars()
        .filter(|c| !c.is_whitespace())
        .collect::<String>();

    let move_error = "Method must not move server. Use `&self` or `&mut self` instead";
    let params_error = &format!("LSP Method must have an argument of type `Method::Params` or `{}::Params`", method_str);
    let return_error = &format!("LSP Request Method must return `Method::Result` or `{}::Result`", method_str);

    if func.sig.inputs.is_empty() {
        return error_at(params_error, func.sig.paren_token.span)
    } else if func.sig.inputs.len() > 2 {
        return error_at("LSP Method must only have 2 arguments: `lsp::Server` and `Method::Params`", func.sig.paren_token.span)
    }

    // Find the `lsp::Server` param
    let mut found_server_arg = false;
    if let Some(receiver) = func.sig.receiver() {
        match receiver {
            FnArg::Receiver(receiver) =>
                if !receiver.reference.is_some() {
                    return error_at(move_error, receiver.span())
                },
            FnArg::Typed(arg) =>
                // At this point, the only type that can consume `self` is Self, which is a Type::Path
                if let Type::Path(_) = arg.ty.as_ref() {
                    return error_at(move_error, arg.ty.span())
                }
        }
        // a Self argument (receiver) is always considered to be of type `lsp::Server`
        found_server_arg = true;
    }
    

    // Find the Method's param and Server param (if was not a Self)
    let mut found_method_arg = false;
    for fnarg in func.sig.inputs.iter_mut() {
        if let FnArg::Typed(arg) = fnarg {
            match arg.ty.as_ref() {
                Type::Path(path) => {
                    let path_str = path.to_token_stream().to_string();
                    let type_path = method.path();

                    if let Some(symbol) = path.path.segments.last() {
                        if !found_server_arg
                        && symbol.ident.to_string() == "Server" {
                            return error_at(move_error, path.span())
                        }
                    }

                    if path_str == quote!{ Method::Params }.to_string()
                    || path_str == quote!{ #type_path::Params }.to_string() {
                        let trait_ = match &method {
                            Method::Request(_) =>
                                quote!{ ::lsp_types::request::Request },
                            Method::Notification(_) =>
                                quote!{ ::lsp_types::notification::Notification },
                        };
                        // Apply Method::Param type
                        arg.ty = syn::parse(quote!{ <#type_path as #trait_>::Params }.into()).unwrap();
                        found_method_arg = true;
                    }
                },
                // If the Server arg was not a Self (receiver), try to find an argument that is type Server
                Type::Reference (TypeReference { elem: ty, .. }) |
                Type::Ptr       (TypePtr       { elem: ty, .. }) =>
                    if let Type::Path(TypePath { path: Path { segments: seg, .. }, .. }) = ty.as_ref() {
                        if let Some(symbol) = seg.last() {
                            if !found_server_arg
                            && symbol.ident.to_string() == "Server" {
                                found_server_arg = true;
                            }
                        }
                    },
                // Server arg must be a reference or a pointer
                _ => {}
            }
        }
    }

    
    if !found_server_arg {
        return error_at("Method must have an argument of type `Server`", func.sig.paren_token.span)
    }
    if !found_method_arg {
        return error_at(params_error, func.sig.paren_token.span)
    }

    // Apply the Method::Result type to the function's return type
    // Method::Result is only for request
    match &method {
        Method::Request(type_path) => match &mut func.sig.output {
            ReturnType::Type(_, ref mut ty) => {
                match ty.as_ref() {
                    Type::Path(path) => {
                        let path_str = path.to_token_stream().to_string();
                        let type_path = method.path();
    
                        if path_str != quote!{ Method::Result }.to_string()
                        && path_str != quote!{ #type_path::Result }.to_string() {
                            return error_at(return_error, path.span())
                        }
                    },
                    _ => return error_at(return_error, ty.span())
                }
    
                // Apply correct type
                *ty.as_mut() = syn::parse(quote!{ <#type_path as ::lsp_types::request::Request>::Result }.into()).unwrap()
            },
            _ => return error_at(return_error, func.sig.ident.span())
        },
        Method::Notification(_) => {
            if let ReturnType::Type(..) = func.sig.output {
                return error_at("Notification methods don't have a return type", func.sig.output.span())
            }
        }
    }

    func.to_token_stream().into()
}

fn error_at(msg: impl AsRef<str>, span: quote::__private::Span) -> TokenStream {
    let msg = format!("{}", msg.as_ref());
    return quote_spanned!{ span=>
        compile_error!(#msg);
    }.into()
}