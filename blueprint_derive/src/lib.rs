use proc_macro::TokenStream;
use syn::{DeriveInput, Data::Struct, Fields::Named};
use quote::quote;


#[proc_macro_derive(Token)]
pub fn token_derive(input: TokenStream) -> TokenStream {
    let ast: DeriveInput = syn::parse(input).unwrap();

    let ident = ast.ident;

    // if struct has no field slice, throw error
    match struct_has_field(ast.data, "src") {
        Ok(true) => {},
        Ok(false) => return quote! { compile_error!("This struct does not have field named *src*") }.into(),
        Err(msg) => return msg
    }

    quote! {
        impl Token for #ident {
            #[inline]
            fn slice(&self) -> &str {
                &self.src[self.span.clone()]
            }
            #[inline]
            fn kind() -> TokenKind {
                TokenKind::#ident
            }
        }
    }.into()
}

/// Derive macro is for [`Token`]s only. All other [`SyntaxNode`]s must implement `Parse` independently.
#[proc_macro_derive(Parse)]
pub fn parse_derive(input: TokenStream) -> TokenStream {
    let ast: DeriveInput = syn::parse(input).unwrap();

    let ident = ast.ident;

    quote! {
        impl Parse for #ident {
            #[inline]
            fn parse(ctx: &mut ParseContext) -> Result<Option<Self>> {
                parse_token(ctx, parse::#ident)
            }
        }
    }.into()
}

#[proc_macro_derive(SyntaxNode)]
pub fn span_derive(input: TokenStream) -> TokenStream {
    let ast: DeriveInput = syn::parse(input).unwrap();

    let ident = ast.ident;

    // if struct has no field span, throw error
    match struct_has_field(ast.data, "span") {
        Ok(true) => {},
        Ok(false) => return quote! { compile_error!("This struct does not have field named *span*") }.into(),
        Err(msg) => return msg
    }

    quote! {
        impl SyntaxNode for #ident {
            #[inline]
            fn span(&self) -> ::std::ops::Range<usize> {
                self.span.clone()
            }
        }
    }.into()
}


/// Returns error if is not a `struct` with **named fields**.
fn struct_has_field(data: syn::Data, name: &str) -> Result<bool, TokenStream> {
    if let Struct(st) = data {
        if let Named(fields) = st.fields {
            for field in fields.named {
                if let Some(ident) = field.ident {
                    if ident.to_string() == name {
                        return Ok(true)
                    }
                }
            }
            return Ok(false)
        }
    }

    Err(quote! { compile_error!("Must be a `struct` with named fields.") }.into())
}